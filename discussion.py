# # Python Lists, Dictionaries, Functions, and Classes

# # [SECTION] List
# # List ae similar to JS Arrays
# # To create a list, the squae brackets ([]) are used

# names = ["John", "Paul", "George", "Ringo"];
# programs = ["developer career", "pi-shape", "short courses"];
# durations = [260, 180, 20];
# truth_values = [True, False, True, True, False];
# print (names);
# print(programs);
# print(durations);
# print (truth_values);
# sample_list = ["Apple", 3, False, "Potato", 4, True];
# print (sample_list);

# # Getting the list size
# # Thre number of elements in a list cn be counted using the len() method
# print (len(programs));

# # Accessing values
# # The index number in the list starts at 0 and ends at (n-1), where is the number of elements


# #Accessing the first element of the list
# print (programs[0]);

# #Accessing the last element of the list
# print (names[-1]);

# # Accessing a range of values
# # list_name[start index: end index -1]
# print (programs[0:2]); # will display index 0 and 1


# # [Section] Mini exercise:
#     # 1. Create a list of names of 5 students
#     # 2. Create a list of grades for the 5 students
#     # 3. Use a loop to iterate through the lists printing in the following format:

# students_name = ["Luffy", "Zoro", "Usopp", "Sanji", "Nami"];
# students_grade = [3000, 1111, 366, 1032, 500];

# for i in range(5):
# 	print (f"The grade of student {students_name[i]} is {students_grade[i]}");



# students_data = {
#     "Luffy": 3000,
#     "Zoro": 1111,
#     "Usopp": 366,
#     "Sanji": 1032,
#     "Nami": 500
# }

# for name, grade in students_data.items():
#     print(f"The grade(bounty) of student {name} is {grade} (Million Beries)");

# #Updating lists
# print(f"Current value : {programs[2]}");

# # Update Value
# programs[2] = "Short Courses";

# # Print the new value
# print(f"New value : {programs[2]}");

# # List Manipulations
# # Adding list items - append() method allows to insert items to the list
# programs.append ("global");
# print (programs);

# # Deleting List items - the "del" keyword can be used to delete elements in the list
# durations.append(360);
# print (durations);

# # Delete last item n the list
# del durations[-1];
# print (durations);

# # Membership Checks - the "in" keyword checks if the element is in the list
# print (20 in durations);	# true
# print (500 in durations);	# false

# #Sorting List - the sort() method sorts the list alphanumerically, ascending by default
# names.sort();
# print(names);

# # Emptying a list- the clear() method is used to empty the content of a list
# test_list  = [1,3,5,7,9]
# print(test_list);
# test_list.clear();
# print(test_list);


# # [SECTION] Dictionaries
# # Dictionaries are used to store data values in key:value pairs. This is similar to Objects in JS

# person1 = {
# 	"name": "Daisy",
# 	"age": 28,
# 	"occupation": "instructors",
# 	"isEnrolled": True,
# 	"subjects": ["Python", "SQL", "Django"]
# }

# print (person1);
# # to get the number of key-value pair, the len() method can be used
# print ( len(person1) );

# #Accessing values in a dictionaries
# # To get the items in a dictionaries, the key name can be referred using square brackets ([])
# print (person1["name"]);

# # The keys() method will return a list of all keys in the dictionary
# print (person1.keys()); #dict_keys(['name', 'age', 'occupation', 'isEnrolled', 'subjects'])

# # The values() method will return a list of all values in the dictionary
# print (person1.values()); #dict_values(['Daisy', 28, 'instructors', True, ['Python', 'SQL', 'Django']])


# # The items() method will return each items in the dictionary as a key-value pair
# print (person1.items()); #dict_items([('name', 'Daisy'), ('age', 28), ('occupation', 'instructors'), ('isEnrolled', True), ('subjects', ['Python', 'SQL', 'Django'])])

# # Adding key-value pairs can be done with either putting a new key and assigning a value or the update method()
# person1["nationality"] = "Filipino";
# print (person1);
# print ("\n")

# person1.update({"fav_food" : "Sinigang"})
# print(person1);

# # Deleting Entries can be done using the pop() method or the del keyword
# person1.pop("fav_food");
# print(person1);

# del person1["nationality"];
# print (person1);

# # The clear() method clears the dictionary
# person2 = {
# 	"name":"John",
# 	"age": 18
# }

# print (person2);
# person2.clear();
# print (person2);


# # Looping through dictionaries
# for key in person1:
# 	print (f"The value of {key} is {person1[key]}");


# # Nested dictionaries
# print ("\n\n");
# person3 = {
# 	"name": "Monica",
# 	"age": 20,
# 	"occupation": "poet",
# 	"isEnrolled": True,
# 	"subjects": ["Python", "SQL", "Django"]
# }

# classRoom = {
# 	"student1" : person1,
# 	"student2" : person3
# }

# print (classRoom);

# print ("\n\n");
# # [Section] Mini Exercise
#     # 1. Create a car dictionary with the following keys:
#     # brand, model, year of make, color
#     # 2. Print the following statement from the details:
#     # "I own a <Brand> <Model> and it was made in <Year of Make>"

# car = {
#     "Brand": "Honda",
#     "Model": "Civic",
#     "Year": 2008,
#     "Color": "silver"
# }

# print(f'I own a {car["Brand"]} {car["Model"]} and it was made in {car["Year"]}');

# # [SECTION] Function
# # Functions are blocks of code used to create functions
# # def <function name> ():

# def my_greeting():
# 	print ("HEllO, User!");

# # Calling/Invoking a function - just specify the function name and provide the value if needed
# my_greeting();

# # PArameters can be added to functions to have more control to what rhe input for that function
# def greet_user(username):
# 	print (f"HEllO, {username}!");

# # Arguments are values that are provided to the function/substituted to the parameter
# greet_user("Charles");
# greet_user("Amy");

# # Return statement - the "return" keyword allow functions to return values

# def addition(num1, num2):
# 	return num1 + num2;

# sum = addition(5,10);
# print (f"the sum is {sum}");


# # [SECTION] LAMBDA function
# # A lambda function is a small, anonymous function that can be used for callback

# greeting = lambda person : f"Hello, {person}"; 

# print (greeting ("Elsie"));
# print (greeting ("Anthony"));

# mult = lambda a, b : a * b;

# print (mult (5, 6));
# print (mult (6, 99));

# lambda_ternary_test = lambda a, b: True if a > b else False
# print (lambda_ternary_test (5, 6));

# square= lambda x : x*x
# num = 5
# print (f"the square of {num} is {square(num)} ");

# print (f"\n\n");

# [SECTION] Classes
# Classes serve as blueprints to describe the concept of an object
# Each object has characteristics(properties) and behaviors(methods)
# To create a class, the "class" keyword is used along with the class name that starts with an upper case

class Car():
	def __init__ (self, brand, model, year_of_make):
		self.brand = brand;
		self.model = model;
		self.year_of_make = year_of_make;

		self.fuel = "Gasoline";
		self.fuel_level = 0;

	# method
	def fill_fuel (self):
		print (f"Current fuel Level : {self.fuel_level}");
		print (f" filling up the fuel tank ....");
		self.fuel_level = 100;
		print (f"New fuel Level : {self.fuel_level}");

	def drive(self, distance):
		self.distance = distance;
		print (f"The car has driven {self.distance} kilometers");
		print (f"The car's fuel level is {self.fuel_level - self.distance}");	

# Creating a new instance is done by calling the class and providing the argument
new_car = Car ("Nissan", "GT-R", "2019");

# Displaying attributes can be done using (.) dot notation
print(f"My car is a {new_car.brand} {new_car.model}");

# Calling methods of the instance
new_car.fill_fuel();

print (f"\n\n");
# Mini Exercise
    # 1. Add a method called drive with a parameter called distance
    # 2. The method would output 2 things
    # "The car has driven <distance> kilometers"
    # "The car's fuel level is <fuel level - distance>"


new_car.drive(45);




